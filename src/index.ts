import { config } from "dotenv";
import { IBundler, Bundler } from "@biconomy/bundler";
import { ChainId } from "@biconomy/core-types";
import {
  BiconomySmartAccount,
  BiconomySmartAccountConfig,
  DEFAULT_ENTRYPOINT_ADDRESS,
} from "@biconomy/account";
import { Wallet, providers, ethers } from "ethers";
import {
  IPaymaster,
  BiconomyPaymaster,
  PaymasterMode,
} from "@biconomy/paymaster";

config();

const bundler: IBundler = new Bundler({
  bundlerUrl: "https://bundler.biconomy.io/api/v2/80001/abc",
  chainId: ChainId.POLYGON_MUMBAI,
  entryPointAddress: DEFAULT_ENTRYPOINT_ADDRESS,
});

const provider = new providers.JsonRpcProvider(
  "https://rpc.ankr.com/polygon_mumbai"
);
const wallet = new Wallet(process.env.PRIVATE_KEY || "", provider);

const biconomySmartAccountConfig: BiconomySmartAccountConfig = {
  signer: wallet,
  chainId: ChainId.POLYGON_MUMBAI,
  bundler: bundler,
};

const paymaster = new BiconomyPaymaster({
  paymasterUrl:
    "https://paymaster.biconomy.io/api/v1/80001/h1JfmkiTQ.6bcc41d1-9922-4264-8a1f-f55bf54d0c11",
});

async function createAccount() {
  let biconomySmartAccount = new BiconomySmartAccount(
    biconomySmartAccountConfig
  );
  biconomySmartAccount = await biconomySmartAccount.init();
  console.log("owner: ", biconomySmartAccount.owner);
  console.log("address: ", await biconomySmartAccount.getSmartAccountAddress());
  return biconomySmartAccount;
}

async function createTransaction() {
  console.log("creating account");

  const smartAccount = await createAccount();

  const transaction = {
    to: "0xD0347a9D15Be1dBfD277c4984A4F0C4e94Cc44D7",
    data: "0x",
    value: ethers.utils.parseEther("0.1"),
  };

  const userOp = await smartAccount.buildUserOp([transaction]);

  userOp.paymasterAndData = (
    await paymaster.getPaymasterAndData(userOp, {
      mode: PaymasterMode.SPONSORED,
      smartAccountInfo: {
        name: "BICONOMY",
        version: "1.0.0",
      },
    })
  ).paymasterAndData;

  console.log(userOp.paymasterAndData);

  const userOpResponse = await smartAccount.sendUserOp(userOp);

  const transactionDetail = await userOpResponse.wait();

  console.log("transaction detail below");
  console.log(transactionDetail);
}

createTransaction();
